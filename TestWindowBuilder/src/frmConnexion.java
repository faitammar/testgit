import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class frmConnexion {

	private JFrame frmGestionDesAlbums;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmConnexion window = new frmConnexion();
					window.frmGestionDesAlbums.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public frmConnexion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionDesAlbums = new JFrame();
		frmGestionDesAlbums.setTitle("Gestion des albums");
		frmGestionDesAlbums.setIconImage(Toolkit.getDefaultToolkit().getImage(frmConnexion.class.getResource("/images/album2.png")));
		frmGestionDesAlbums.setBounds(100, 100, 450, 300);
		frmGestionDesAlbums.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frmGestionDesAlbums.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(2, 2, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Pr\u00E9nom");
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 18));
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setFont(new Font("SansSerif", Font.PLAIN, 18));
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nom");
		lblNewLabel_1.setFont(new Font("Serif", Font.PLAIN, 14));
		panel.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Serif", Font.PLAIN, 14));
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		frmGestionDesAlbums.getContentPane().add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0), 2));
		
		JPanel panel_1 = new JPanel();
		frmGestionDesAlbums.getContentPane().add(panel_1, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Valider");
		btnNewButton.setMnemonic('v');
		btnNewButton.setToolTipText("Valider");
		btnNewButton.setIcon(new ImageIcon(frmConnexion.class.getResource("/images/check.png")));
		panel_1.add(btnNewButton);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setIcon(new ImageIcon(frmConnexion.class.getResource("/images/icon_quit.gif")));
		btnQuitter.setMnemonic('q');
		panel_1.add(btnQuitter);
	}

}

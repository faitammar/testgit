import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;

public class connexion2 extends JFrame {

	private JPanel contentPane;
	private JLabel lblNomDutilisateur;
	private JButton btnNewButton;
	private JButton btnValider;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					connexion2 frame = new connexion2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public connexion2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNomDutilisateur());
		contentPane.add(getBtnNewButton());
		contentPane.add(getBtnValider());
		contentPane.add(getLblNewLabel());
		contentPane.add(getLblNewLabel_1());
	}
	private JLabel getLblNomDutilisateur() {
		if (lblNomDutilisateur == null) {
			lblNomDutilisateur = new JLabel("Nom d'utilisateur");
			lblNomDutilisateur.setBounds(55, 85, 122, 44);
		}
		return lblNomDutilisateur;
	}
	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("New button");
			btnNewButton.setBounds(217, 95, 97, 25);
		}
		return btnNewButton;
	}
	private JButton getBtnValider() {
		if (btnValider == null) {
			btnValider = new JButton("Valider");
			btnValider.setBounds(133, 152, 97, 25);
		}
		return btnValider;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Connexion au programme");
			lblNewLabel.setIcon(new ImageIcon(connexion2.class.getResource("/images/Key.png")));
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 21));
			lblNewLabel.setBounds(0, 42, 420, 53);
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("New label");
			lblNewLabel_1.setIcon(new ImageIcon(connexion2.class.getResource("/images/ban.png")));
			lblNewLabel_1.setBounds(0, 13, 432, 240);
		}
		return lblNewLabel_1;
	}
}
